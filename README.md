# resources

This repository is used for stashing all the resources that are relevant for ÅPENKILDEs business.

## License
All files in this repository is available under the Creative Commons Attribution-ShareAlike 4.0 International.
